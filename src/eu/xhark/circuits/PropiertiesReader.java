package eu.xhark.circuits;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.example.myapp.R;
import eu.xhark.circuits.logic.Circuit;

/**
 * Created by martin
 * on 25/04/14.
 */
public class PropiertiesReader extends Activity {
    TextView tvName, tvSport, tvNumObs, tvLastID;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.propierties_activity);
        setTitle(R.string.circuits);
        Circuit circuit = (Circuit) getIntent().getExtras().get("circuit");
        tvName = (TextView) findViewById(R.id.tvName);
        tvSport = (TextView) findViewById(R.id.tvSport);
        tvNumObs = (TextView) findViewById(R.id.tvNumObs);
        tvLastID = (TextView) findViewById(R.id.tvLastID);
        tvName.setText(circuit.getName());
        tvSport.setText(" " + circuit.getSport());
        tvNumObs.setText(" " + circuit.getObstacles().size());
        if ((circuit.getObstacles().isEmpty())) {
            tvLastID.setVisibility(View.GONE);
        } else {
            tvLastID.setText(" " + circuit.getObstacles().get(circuit.getObstacles().size() - 1).getId());
        }

    }
}