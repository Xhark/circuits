package eu.xhark.circuits;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.myapp.R;
import eu.xhark.circuits.Utils.FileManager;
import eu.xhark.circuits.logic.Circuit;
import eu.xhark.circuits.logic.Obstacle;
import eu.xhark.circuits.logic.Sport;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CircuitManager extends Activity {

    CircuitView circuitView;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    //La lista que forma el menu
    ListView obstaclesList;

    static Circuit circuit;
    String sportKey = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.circuit_layout);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        circuitView = (CircuitView) findViewById(R.id.circuitControl);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        obstaclesList = (ListView) findViewById(R.id.obstaclesList);
        setDrawerIcon();

        Intent intent= getIntent();
        /*
         * Recupera el circuito que se le pueda estar mandando.
         */

        circuit = (Circuit) intent.getExtras().get("circuit");
        sportKey = (String) intent.getExtras().get("sportKey");

        /*
         * Crea un array de obstáculos vacíos para añadir en caso de que el circuito este vacío
         */
        if (circuit == null) {
            ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
            circuit = new Circuit(obstacles, "empty", new Sport());
        }
        if (sportKey == null) {
            sportKey = circuit.getSport().getKey();
        }
        setMenuItems();
        setTitle(circuit.getName());
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.obstacles) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(getResources().getString(R.string.app_name));
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Obstacles");
                invalidateOptionsMenu();
            }
        };

        drawerLayout.setDrawerListener(toggle);
        circuitView.setCircuit(circuit);//Carga el circuito
        // circuitView.loadViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.circuit_menu, menu);

        /*MenuItem item = menu.findItem(R.id.menu_item_share);

        // Fetch and store ShareActionProvider
        ShareActionProvider mShareActionProvider = (ShareActionProvider) item.getActionProvider();


        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        File f = new File(FileManager.PATH + circuit.getName() + ".obj");
        Log.d("SHARING", f.getAbsolutePath());
        if (f.exists()) {
            Log.d("FILE", f.getAbsolutePath());
        }
        Uri uri = Uri.fromFile(f);
        Log.d("URI", uri.toString());
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("text/plain");
        Intent.createChooser(shareIntent, "Share via");
        mShareActionProvider.setShareIntent(shareIntent);*/
        return true;


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Determina las diferente funciones de las opciones del menu superior izquierdo
     *
     * @param item
     */
    public void settingsMenuItem(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveItem:
                FileManager.saveToFile(circuit.getName(), circuitView);
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                break;
            case R.id.loadItem:
                circuitView.setCircuit(FileManager.loadFromFile(circuit.getName()));
                Toast.makeText(this, "Loaded", Toast.LENGTH_SHORT).show();
                break;
            case R.id.saveAsItem:
                String infService = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(infService);
                View inflator = li.inflate(R.layout.save_input_dialog, null);
                final TextView etName = (TextView) inflator.findViewById(R.id.et_name);


                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("Save as");
                dialog.setView(inflator);

                dialog.setPositiveButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String fileName = etName.getText().toString();
                        circuit.setName(fileName);
                        FileManager.saveToFile(fileName, circuitView);
                    }
                });

                dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.create();
                dialog.show();
                break;
            case R.id.properties:

                //actualiza el fichero y con ello el objeto
                FileManager.saveToFile(circuit.getName(), circuitView);
                circuitView.setCircuit(FileManager.loadFromFile(circuit.getName()));
                Intent i = new Intent(getApplicationContext(), PropiertiesReader.class);
                i.putExtra("circuit", circuit);
                startActivity(i);

                break;
        }
    }

    public void setDrawerIcon() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.obstacles) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(getResources().getString(R.string.app_name));
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Menu");
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(toggle);
    }

    /**
     * Añade al list view del menu desplegable tantas entradas como imágenes existan en la carpeta drawable.
     */
    public void setMenuItems() {
        Field[] fields = R.drawable.class.getFields();
        ArrayList<String> names = new ArrayList<String>();
        for (Field field : fields) {
            Log.d("NAME", field.getName() + " " + sportKey);
            if (field.getName().toUpperCase().contains(sportKey)) {
                names.add(field.getName().substring(sportKey.length() + 1));
            }
        }
        obstaclesList.setAdapter(new MenuAdapter(names));

        // Añade al CircuitView una vista que representa el obstaculo y al objeto Circuit un objeto Obstacle
        obstaclesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = ((TextView) view.findViewById(R.id.menuItemText)).getText().toString().toLowerCase();
                circuitView.setView(20, 20, name);
                //refresh
                FileManager.saveToFile(circuit.getName(), circuitView);
                circuitView.setCircuit(FileManager.loadFromFile(circuit.getName()));
                drawerLayout.closeDrawers();
            }
        });
        //obstaclesList.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;

    }

    /**
     * Adapter para rellenar el menu
     */
    public class MenuAdapter extends ArrayAdapter<String>{
        /**
         * Recive una lista de nombres de obstaculos
         * @param obstacles
         */
        public MenuAdapter(List<String> obstacles) {
            super(getApplicationContext(), R.layout.menu_list_item);
            for (String s : obstacles) {
                add(s);
            }

        }

        /**
         * Metodo sobreescrito que define cada item como una LinearLayout formado por una ImageView(miniatura del obstaculo) y un TextView(nombre del obstaculo)
         * @param position
         * @param convertView
         * @param parent
         * @return LinearLayout layout la vista que forma cada elemento del menu
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.menu_list_item, parent, false);
            final String name = getItem(position);

            TextView menuItemText = (TextView) layout.findViewById(R.id.menuItemText);
            //Primera letra en mayuscula
            menuItemText.setText(name.substring(0,1).toUpperCase()+name.substring(1).toLowerCase());
            ImageView itemImg = (ImageView) layout.findViewById(R.id.menuItemImg);

            String imgName = circuit.getSport().getKey().toLowerCase() + "_"+ name;
            try {
                itemImg.setImageResource(R.drawable.class.getField(imgName).getInt(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            return layout;
        }
    }
}