package eu.xhark.circuits.logic;

import java.io.Serializable;

/**
 * Created by mamrtin on 14/04/14.
 */
public class Obstacle implements Serializable {
    private int x, y;
    private static int idMax = 0;
    private int idObstacle;
    private String img;
    private int height;
    private int width;
    private int angle;

    public Obstacle(int x, int y, int height, int width, String img) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.img = img;
        this.angle = 180;
        idObstacle = idMax++;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getId() {
        return idObstacle;
    }

    public void setId(int idObstacle){
        this.idObstacle =idObstacle;
    }

    public String getImg() {
        return img;
    }

    public void setIdObstacle(int idObstacle) {
        this.idObstacle = idObstacle;
    }

    public static int getIdMax(){
        return idMax;
    }

    public static void setIdMax(int id){
        idMax=id;
    }

    public static void setZero() {
        idMax = 0;
    }
}
