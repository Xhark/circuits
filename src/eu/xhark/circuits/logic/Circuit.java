package eu.xhark.circuits.logic;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Martin on 14/04/14.
 */
public class Circuit implements Serializable {

    private ArrayList<Obstacle> obstacles;
    private String name;
    private Sport sport;

    public Circuit(ArrayList<Obstacle> obstacles, String name, Sport sport) {
        this.obstacles = obstacles;
        this.name = name;
        this.sport = sport;
    }


    public Circuit() {
        obstacles = new ArrayList<Obstacle>();
        this.name = "";
        this.sport = new Sport("", "");
        ;
    }

    public ArrayList<Obstacle> getObstacles() {
        return obstacles;
    }

    public void setObstacles(ArrayList<Obstacle> obstacles) {
        this.obstacles = obstacles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public void addObstacle(Obstacle obstacle) {
        if (!obstacles.isEmpty()) {
            obstacle.setIdObstacle(obstacles.get(obstacles.size() - 1).getId() + 1);
        }
        obstacles.add(obstacle);
    }

    /**
     * Devuelve el obstaculo del arrayList con el mismo id que el indicado
     *
     * @param id
     * @return
     */
    public Obstacle findObstacleById(int id) {
        for (Obstacle obs : obstacles) {
            if (obs.getId() == id) {
                return obs;
            }
        }
        return null;
    }


}
