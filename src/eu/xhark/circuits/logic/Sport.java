package eu.xhark.circuits.logic;

import java.io.Serializable;

/**
 * Created by Martin
 * on 01/05/14.
 */
public class Sport implements Serializable {
    String name;
    String key;

    public Sport(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public Sport() {
        this.name = "";
        this.key = "";
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString(){
        return name;
    }
}
