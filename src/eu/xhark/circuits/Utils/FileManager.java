package eu.xhark.circuits.Utils;

import android.os.Environment;
import android.util.Log;
import eu.xhark.circuits.CircuitView;
import eu.xhark.circuits.logic.Circuit;

import java.io.*;

/**
 * Created by Martin
 * on 20/04/14.
 */
public class FileManager {


    public static final String PATH = Environment.getExternalStorageDirectory() + File.separator;

    /**
     * Devuelve el circuito serializado dentro del archivo que se le indica.
     *
     * @param fileName Nombre del archivo;
     * @return circuit Circuito almacenado en el archivo
     */
    public static Circuit loadFromFile(String fileName) {
        try {
            File f = new File(PATH + fileName + ".obj");
            FileInputStream fileIn = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fileIn);
            Circuit circuit = (Circuit) ois.readObject();
            ois.close();

            return circuit;


        } catch (FileNotFoundException e) {
            Log.e("Exception IO", e.toString());
        } catch (IOException e) {
            Log.e("Exception IO", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveToFile(String fileName, CircuitView circuitView) {
        try {
            File f = new File(PATH + fileName + ".obj");
            Log.d("FILE SAVE", f.getAbsolutePath());
            FileOutputStream fileOut = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fileOut);
            oos.writeObject(circuitView.getCircuit());
            oos.close();
        } catch (FileNotFoundException e) {
            Log.e("File Not Fond Ex", e.toString());
        } catch (IOException e) {
            Log.e("Exception IO", e.toString());
        }
    }

    public static boolean deleteCircuitFile(File file) {
        return file.delete();
    }
}
