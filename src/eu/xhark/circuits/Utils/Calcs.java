package eu.xhark.circuits.Utils;

/**
 * Created by martin
 * on 24/04/14.
 */
public class Calcs {

    public static int optimicedHeight(int w, int h, int angle) {
        double trans = Math.PI / 180;
        return (int) Math.ceil(Math.abs(h * Math.cos(angle * trans)) + Math.abs(w * Math.cos((90 - angle) * trans)));
    }

    public static int optimicedWidth(int w, int h, int angle) {
        double trans = Math.PI / 180;
        return (int) Math.ceil(Math.abs(h * Math.sin(angle * trans)) + Math.abs(w * Math.sin((90 - angle) * trans)));
    }
}
