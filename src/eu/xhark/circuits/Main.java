package eu.xhark.circuits;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.myapp.R;
import eu.xhark.circuits.Utils.FileManager;
import eu.xhark.circuits.logic.Circuit;
import eu.xhark.circuits.logic.Obstacle;
import eu.xhark.circuits.logic.Sport;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by martin
 * on 18/04/14.
 */
public class Main extends Activity {

    /*
      TODO Recargar los circuitos al borrar
    */
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        listView = (ListView) findViewById(R.id.circuitsListView);

        loadCircuits();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        loadCircuits();
    }

    public void mainMenuItem(MenuItem item) {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(infService);
        View inflator = li.inflate(R.layout.create_input_dialog, null);
        final TextView etName = (TextView) inflator.findViewById(R.id.et_name);
        final Spinner spSports = (Spinner) inflator.findViewById(R.id.spSorts);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Create Circuit");
        dialog.setView(inflator);

        dialog.setPositiveButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fileName = etName.getText().toString();
                String[] s = getResources().getStringArray(R.array.sportsKey);
                String sportKey = s[(spSports.getSelectedItemPosition())];
                Circuit newCircuit = new Circuit(new ArrayList<Obstacle>(), fileName, new Sport(spSports.getSelectedItem().toString(), sportKey));
                //TODO Guardar en fichero el circuito nuevo vacío o esperar a guardar en el manager??
                Intent i = new Intent(getApplicationContext(), CircuitManager.class);
                Log.d("Sport Key", sportKey);
                i.putExtra("circuit", newCircuit);
                i.putExtra("sportKey", sportKey);
                startActivityForResult(i, 23);

            }
        });

        dialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.create();
        dialog.show();
    }


    public ArrayList<File> getListOfCircuitsFromFile() {
        Environment.getExternalStorageDirectory();
        File f = Environment.getExternalStorageDirectory();
        ArrayList<File> files = new ArrayList<File>();
        for (File file : f.listFiles()) {
            if (file.isFile()) {
                if (file.getName().endsWith(".obj")) {
                    files.add(file);
                }
            }
        }
        return files;

    }


    /**
     * Recorre el fichero donde se guardan los circuitos serializados y rellena un listView con los nombres de los que encuentre.
     * Le añade un clickListener para abrir cada fichero y un longClickListener para eliminarlo.
     */
    public void loadCircuits() {
        ArrayList<String> names = new ArrayList<String>();

        final File FILE = new File(FileManager.PATH);
        if (FILE != null) {
            for (File f : getListOfCircuitsFromFile()) {
                //Añade el nombre quitándole la extensión
                String[] name = f.getName().split("\\.");
                names.add(name[0]);
            }

        }


        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, names));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), CircuitManager.class);
                if (FILE != null) {
                    /*
                     * Envía como extra el circuito guardado en el archivo con el mismo nombre que la sección del listview donde  se clicka.
                     */
                    Circuit c = FileManager.loadFromFile(((TextView) view).getText().toString());
                    Log.d("Circuit", c.getName());
                    i.putExtra("circuit", c);
                }
                startActivityForResult(i, 23);

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                String viewText = ((TextView) view).getText().toString();


                final File f = new File(FileManager.PATH + viewText + ".obj");
                if (f.exists()) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(parent.getContext());
                    dialog.setTitle(getString(R.string.delete_circuit_title) + " " + f.getName() );
                    dialog.setMessage(R.string.delete_circuit_msg);
                    dialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int button) {
                            FileManager.deleteCircuitFile(f);
                            loadCircuits();
                        }
                    });

                    dialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int button) {

                        }
                    });

                    dialog.show();
                    loadCircuits();
                    return true;
                }
                return false;
            }
        });
    }
}