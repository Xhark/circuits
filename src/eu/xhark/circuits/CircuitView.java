package eu.xhark.circuits;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.example.myapp.R;
import eu.xhark.circuits.Utils.Calcs;
import eu.xhark.circuits.Utils.FileManager;
import eu.xhark.circuits.logic.Circuit;
import eu.xhark.circuits.logic.Obstacle;

/**
 * Created by martin
 * on 11/04/14.
 */
public class CircuitView extends LinearLayout {

    Context context;

    private CircuitView me = this;
    private ViewGroup field;
    private SeekBar seekBar;
    private ImageView img;
    private Circuit circuit;
    private ImageView selectImg;
    private Obstacle selectObs;


    public CircuitView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CircuitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ViewGroup getField() {
        return field;
    }

    private void init() {


        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        li.inflate(R.layout.circuit_view, this, true);
        circuit = new Circuit();
        field = (ViewGroup) findViewById(R.id.field);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int w = selectImg.getWidth();
                int h = selectImg.getHeight();
                int x = 0;
                int y = 0;

                //Log.d("Before",w + " "+ h);

                selectImg.getLayoutParams().height = selectObs.getWidth();
                //selectImg.setMinimumHeight(w);

                int optH = Calcs.optimicedHeight(selectObs.getWidth(), selectObs.getHeight(), selectObs.getAngle());
                int optW = Calcs.optimicedWidth(selectObs.getWidth(), selectObs.getHeight(), selectObs.getAngle());
                /*selectImg.getLayoutParams().height = optH;
                selectImg.getLayoutParams().width = optW;*/

                selectImg.setScaleType(ImageView.ScaleType.MATRIX);   //required
                Matrix matrix = new Matrix();
                Log.d("Data", w / 2 + " " + selectObs.getWidth() / 2 + " " + h / 2 + " " + selectObs.getHeight() / 2);
                matrix.postTranslate(w / 2 - selectObs.getWidth() / 2, h / 2 - selectObs.getHeight() / 2);
                selectImg.setImageMatrix(matrix);
                matrix.postRotate(seekBar.getProgress(), w / 2, h / 2);
                selectObs.setAngle(seekBar.getProgress());
                selectImg.setImageMatrix(matrix);
                // matrix.postTranslate(0, -(h / 2 - selectObs.getHeight() / 2));

                //selectImg.setImageMatrix(matrix);  //required
                selectImg.getImageMatrix();

                // Log.d("After",optH+" "+optW+" Image dimens "+ selectImg.getHeight()+" x "+selectImg.getWidth());

               /* selectImg.getLayoutParams().height = w;
                selectImg.getLayoutParams().width = h;*/
                field.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //selectImg.setBackgroundColor(Color.GREEN);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        //loadViews();
        ImageView v = new ImageView(context);
        try {
            v.setImageResource(R.drawable.class.getField("ic_drawer").getInt(null));

        } catch (NoSuchFieldException e) {
            Log.e("asdas", "Nusuch");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            Log.e("asdas", "Illegal");
            e.printStackTrace();
        }
        field.addView(v);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setVisibility(GONE);
            }
        });

    }

    /**
     * @param x
     * @param y
     * @param img
     */
    public void setView(int x, int y, String img) {
        //Forma el nombre del drawable que busca y lo localiza mediante reflection
        img = circuit.getSport().getKey().toLowerCase() + "_" + img;
        ImageView imgView = new ImageView(context);
        try {
            imgView.setImageResource(R.drawable.class.getField(img).getInt(null));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        imgView.setOnTouchListener(new touchNDragListener(imgView, context, field, circuit));
        try {
            BitmapDrawable bd = (BitmapDrawable) this.getResources().getDrawable(R.drawable.class.getField(img).getInt(null));
            int height = bd.getBitmap().getHeight();
            int width = bd.getBitmap().getWidth();


            Obstacle obs = new Obstacle(x, y, height, width, img);
            circuit.addObstacle(obs);
            imgView.setTag(obs.getId());
            imgView.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    selectImg = (ImageView) v;
                    selectObs = circuit.findObstacleById(Integer.parseInt(v.getTag().toString()));
                    seekBar.setProgress(selectObs.getAngle());
                    Toast.makeText(context, selectObs.getId() + " " + selectObs.getWidth() + "  " + selectObs.getHeight(), Toast.LENGTH_SHORT).show();

                    seekBar.setVisibility(VISIBLE);
                    return false;
                }
            });
            field.addView(imgView);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imgView.getLayoutParams();
            params.leftMargin = x;
            params.topMargin = y;
            imgView.setLayoutParams(params);
            field.invalidate();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
     * Recoge el imageView creado por loadViews(), le asigna las posiciones y la añade al layout.
     *
     * @param img
     * @param x
     * @param y
     */
    public void setObstacle(final ImageView img, int x, int y, int h, int obstacleID) {
        img.setOnTouchListener(new touchNDragListener(img, context, field, circuit));


        //circuit.addObstacle(obs);
        img.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                selectObs = circuit.findObstacleById(Integer.parseInt(v.getTag().toString()));
                selectImg = (ImageView) v;
                Toast.makeText(context, selectObs.getId() + " " + selectObs.getWidth() + "  " + selectObs.getHeight(), Toast.LENGTH_SHORT).show();

                //selectImg.setBackgroundColor(Color.GREEN);
                seekBar.setVisibility(VISIBLE);

                seekBar.setProgress(selectObs.getAngle());
                return false;
            }
        });
        field.addView(img);
        img.getLayoutParams().height = h;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img.getLayoutParams();
        params.leftMargin = x;
        params.topMargin = y;
        img.setLayoutParams(params);
        TextView id = new TextView(getContext());
        id.setText(obstacleID+"");
        id.setLayoutParams(params);
        field.addView(id);
        field.invalidate();


    }

    /**
     * Recorre el array de obstaculos creando una ImageView por cada obstaculo y añadiendo un id ascendente
     * a cada vista.
     * Llama a setObstacle(ImageView,int x, int y)
     */
    public void loadViews() {

        if (circuit == null) {
            return;
        }
        Obstacle.setZero();
        int i = 0;
        for (Obstacle obs : circuit.getObstacles()) {
            img = new ImageView(context);
            try {
                //Establece la imagen del imageView que representa el Obstaculo
                img.setImageResource(R.drawable.class.getField(obs.getImg()).getInt(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            img.setTag(obs.getId());
            //  img.getLayoutParams().height = img.getWidth();
            img.setScaleType(ImageView.ScaleType.MATRIX);   //required

            Matrix matrix = new Matrix();


            matrix.postRotate(obs.getAngle(), obs.getWidth() / 2, obs.getHeight() / 2);
            img.setImageMatrix(matrix);  //required

            matrix.postTranslate(0, obs.getWidth() / 2 - obs.getHeight() / 2);
            img.setImageMatrix(matrix);


            img.getImageMatrix();

            int measure = obs.getWidth();
            if(obs.getHeight()>=obs.getWidth()){
                measure = obs.getHeight();
            }
            setObstacle(img, obs.getX(), obs.getY(), measure, obs.getId());
            i++;
        }

    }

    /**
     * Establece el circuito sobre el campo. Elimina todas las vistas que pudiesen estar en el.Llama al metodo
     * loadViews()
     *
     * @param circuit
     */
    public void setCircuit(Circuit circuit) {
        this.circuit = circuit;
        field.removeAllViews();
        loadViews();
        field.invalidate();
    }

    public Circuit getCircuit() {
        return circuit;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);

        this.setMeasuredDimension(parentWidth, parentHeight);
    }


class touchNDragListener implements View.OnTouchListener {

    private int xDelta;
    private int yDelta;
    private Obstacle modObs;
    private View view;
    Context context;
    private ViewGroup field;
    private Circuit circuit;
    GestureDetector gestureDetector;

    public touchNDragListener(View view, Context context, ViewGroup field, Circuit circuit) {
        this.view = view;
        this.context = context;
        this.field = field;
        this.circuit = circuit;
        gestureDetector = new GestureDetector(context, new GestureListener(this.view, circuit));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();

        modObs = circuit.findObstacleById(Integer.parseInt(v.getTag().toString()));
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams Params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                xDelta = X - Params.leftMargin;
                yDelta = Y - Params.topMargin;

                break;
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_POINTER_DOWN:

                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = X - xDelta;
                layoutParams.topMargin = Y - yDelta;
                RelativeLayout rl = (RelativeLayout) view.getParent();

                if (rl == null) {//Comprueba null por que en doble tap pasa por aquí y al eliminar la vista puede dar null.
                    return true;
                }

                if (((Y - yDelta) >= rl.getHeight() - view.getHeight())) {
                    layoutParams.topMargin = rl.getHeight() - view.getHeight();
                }
                if (((X - xDelta) >= rl.getWidth() - view.getWidth())) {
                    layoutParams.leftMargin = rl.getWidth() - view.getWidth();
                }
                if ((Y - yDelta) <= 0) {
                    layoutParams.topMargin = 0;
                }
                if ((X - xDelta) <= 0) {
                    layoutParams.leftMargin = 0;
                }
                modObs.setX(X - xDelta);
                modObs.setY(Y - yDelta);
                layoutParams.rightMargin = -view.getHeight();
                layoutParams.bottomMargin = -view.getWidth();

                view.setLayoutParams(layoutParams);
                return true;

        }
        field.invalidate();
        return gestureDetector.onTouchEvent(event);

    }
}

class GestureListener extends GestureDetector.SimpleOnGestureListener {
    View tappedImg;
    Circuit circuit;

    public GestureListener(View img, Circuit circuit) {
        super();
        tappedImg = img;
        this.circuit = circuit;

    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) tappedImg.getLayoutParams();
        ((ViewManager) tappedImg.getParent()).removeView(tappedImg);
        Obstacle obs = circuit.findObstacleById(Integer.parseInt(tappedImg.getTag().toString()));
        circuit.getObstacles().remove(obs);
        seekBar.setVisibility(GONE);
        for(Obstacle o : circuit.getObstacles()){
            if(o.getId() > obs.getId()){
                o.setId(o.getId()-1);
            }
        }
        Obstacle.setIdMax(Obstacle.getIdMax()-1);
        Log.d("TAP", "Tapped on" + obs.getId() + "," + e.getY());

        //refresh
        FileManager.saveToFile(circuit.getName(), me);
        setCircuit(FileManager.loadFromFile(circuit.getName()));
        return true;
    }
}
}